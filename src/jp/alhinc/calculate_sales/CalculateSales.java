package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NOT_CONSECUTIVE = "売上ファイル名が連番になっていません";
	private static final String DIGITS_OVER = "合計金額が10桁を超えました";
	private static final String BRANCH_NUM_UNKNOWN = "の支店コードが不正です";
	private static final String COMMODITY_NUM_UNKNOWN = "の商品コードが不正です";
	private static final String RCDFILE_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// コマンドライン引数が1つ設定されているか確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと支店の売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと商品の売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		
		// 支店定義ファイル及び商品定義ファイルのフォーマット確認処理の文字パターン
		String branchCharacter = "[0-9]{3}";
		String commodityCharacter = "(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8}";
		
		
		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchCharacter)) {
			return;
		}
		
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityCharacter)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		// 売上ファイルのまとめ
		File[] files = new File(args[0]).listFiles();
		ArrayList<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd")) {
				rcdFiles.add(files[i]);
			}
		}
		
		// 売上ファイルが連番になっているかチェック
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_CONSECUTIVE);
				return;
			}
		}
		
		// 2-2 まとめた売上ファイルの読込、支店コード・売上額の抽出、売上額の加算		
		BufferedReader br = null;		
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				ArrayList<String> rcdValues = new ArrayList<>();
				while((line = br.readLine()) != null) {
					rcdValues.add(line);					
				}
				
				// 売上ファイルのフォーマット確認
				if(rcdValues.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + RCDFILE_INVALID_FORMAT);
					return;
				}
				
				// 売上金額が数字であるかの確認
				if(!rcdValues.get(2).matches("[0-9]*")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				
				long rcdSale = Long.parseLong(rcdValues.get(2));
				
				// 売上ファイルの支店コードが支店定義ファイルに存在するか確認
				if(!branchNames.containsKey(rcdValues.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCH_NUM_UNKNOWN);
					return;
				}
				
				// 売上ファイルの商品コードが商品定義ファイルに存在するか確認
				if(!commodityNames.containsKey(rcdValues.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITY_NUM_UNKNOWN);
					return;
				}
				
				Long branchSale = branchSales.get(rcdValues.get(0)) + rcdSale;
				Long commoditySale = commoditySales.get(rcdValues.get(1)) + rcdSale;
				
				// 売上金額の合計金額の桁数を確認
				if(branchSale >= 10000000000L || commoditySale >= 10000000000L){
					System.out.println(DIGITS_OVER);
					return;
				}
				branchSales.put(rcdValues.get(0), branchSale);
				commoditySales.put(rcdValues.get(1), commoditySale);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> fileNames, Map<String, Long> fileSales, String JudgeCharacter) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			
			// 支店定義ファイル及び商品定義ファイルの有無を確認
			if(!file.exists()) {
				System.out.println(fileName + FILE_NOT_EXIST);
				return false;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] splitFiles = line.split(",");
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				// 支店定義ファイル及び商品定義ファイルのフォーマットを確認
				if((splitFiles.length != 2) || (!splitFiles[0].matches(JudgeCharacter))) {
					System.out.println(fileName + FILE_INVALID_FORMAT);
					return false;
				}
				// 取得した配列の値をそれぞれのMapに保存する
				fileNames.put(splitFiles[0], splitFiles[1]);
				fileSales.put(splitFiles[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> fileNames, Map<String, Long> fileSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String fileNum : fileNames.keySet()) {
				bw.write(fileNum + "," + fileNames.get(fileNum) + "," + fileSales.get(fileNum));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
